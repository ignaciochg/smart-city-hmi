import PySimpleGUI as sg
import os, sys
from pymodbus.client.sync import ModbusTcpClient



state_maximized = False
PLC_IP = "192.168.1.2"
PLC_PORT = "502"
REMOTE_BIND_PORT = "1234"



class EventHandler():
    def __init__(self, window, functions):
        self.window = window
        self.functions = functions

    def handle_it(self, event, values):
        try:
            self.functions[event][0](event, values, self.functions[event][1])
        except KeyError:
            print(f"Could not find Handler for Event: {event}")
            # pass

        


def change_modbus_client(event,values,args):
    global PLC_IP, PLC_PORT, client
    try:
        new_client = ModbusTcpClient(PLC_IP, int(PLC_PORT))
        client.close()
        client = new_client
    except:
        print("Could not init modbus client. Defaulting to previous values")
    




functions = {
                "traffic_normal": (print, ("hello"))
            }








sg.theme('Dark Amber')  # Let's set our own color theme

trafic_lights = [ 
            [sg.Button("Normal",key="traffic_normal"),
            sg.Button("Off",key="traffic_off"),
            sg.Button("Stop",key="traffic_stop")]
         ]

train_control = [ 
            [sg.Button("On",key="train_on"),
            sg.Button("Off",key="train_off")]
         ]
train_direction = [ 
         [sg.Button("<---",key="train_reverse"),
         sg.Button("0",key="train_0"),
         sg.Button("--->",key="train_forward")]
      ]

# traffic_layout = [[]

layout1 = [ [sg.Frame("Traffic Light Mode",trafic_lights,expand_x = True,expand_y = True)],
            [sg.Frame("Train Enable",train_control,expand_x = True,expand_y = False), sg.Frame("Train Direction",train_direction,expand_x = True,expand_y = True)],
            [sg.Input()]
         ]
layout2 = [ 
            [sg.Text('2 is a very basic PySimpleGUI layout')],
            [sg.Input()]
         ]


settings_screen = [ 
            # [sg.Text('Settings:')],
            [sg.Text('Minimize/Maximize:'),sg.Button("Toggle", key="nor_max")],
            [sg.Text("Debug verbosity:"),sg.Combo(["Off","Low","Mideum","High"], default_value="Off", enable_events=True, auto_size_text=True ,key="verbose_level")],
            [sg.Button('Exit')]
         ]

remote_settings = [ 
            [sg.Text("Enable:"), sg.Checkbox("", enable_events = True, key = "enable_remote")],

            [sg.Text("Bind port:"), sg.Input(default_text=REMOTE_BIND_PORT, key="remote_bind_port", size=(5,None))]
         ]
connection_settings = [[sg.Text("IP:"),
                        sg.Input(default_text = PLC_IP,size=(15,None), enable_events = True, do_not_clear = True, key = None),
                        sg.Text("Port:"),
                        sg.Input(default_text = PLC_PORT,size=(5,None), enable_events = True, do_not_clear = True, key = None)
            ]]
settings_layout = [[sg.Frame("Connection",connection_settings,expand_x = True,expand_y = True), sg.Frame("HMI",settings_screen,expand_x = True,expand_y = True)],
                
                [sg.Frame("Web Control",remote_settings,expand_x = False,expand_y = False)]
            ]



# selector_layout = [[sg.Button('Cycle Layout'), sg.Button('1'), sg.Button('2'), sg.Button('Settings')]]
selector_layout = [[sg.Button('Controls', key="1"), sg.Button('Hacks',key="2"), sg.Button('Settings',key="0")]]

# tabs_layout= [[sg.Tab("test1",layout2)]]
layout = [  #[sg.TabGroup(tabs_layout)],
            [sg.Column(selector_layout, element_justification='center', vertical_alignment='top', expand_x=True)],
            [sg.Column(layout1, key='-COL1-'), sg.Column(layout2, visible=False, key='-COL2-'), sg.Column(settings_layout, visible=False, key='-COL0-')]]






window = sg.Window('Smart City HMI by Ignacio Astaburuaga', layout, no_titlebar=False, location=(0,0),finalize=True, keep_on_top=True,resizable=True) # size=(800,600), 
eh = EventHandler(window, functions)
try:
    client = ModbusTcpClient(PLC_IP, int(PLC_PORT))
except:
    print("Could not init modbus client.")
    sys.exit(1)

if state_maximized:
    window.maximize()

current_layout = 1  # The currently visible layout
while True:
    event, values = window.read()   # Read the event that happened and the values dictionary
    print(event, values)
    if event == sg.WIN_CLOSED or event == 'Exit':     # If user closed window with X or if user clicked "Exit" button then exit
        break

    elif event == "nor_max":
        if state_maximized:
            window.normal()
        else:
            window.maximize()
        state_maximized = not state_maximized

    # if event == 'Cycle Layout':
    #     window[f'-COL{current_layout}-'].update(visible=False)
    #     current_layout = current_layout + 1 if current_layout < 3 else 1
    #     window[f'-COL{current_layout}-'].update(visible=True)
    elif event in '0123':
        window[f'-COL{current_layout}-'].update(visible=False)
        current_layout = int(event)
        window[f'-COL{current_layout}-'].update(visible=True)

    else:
        eh.handle_it(event, values)

window.close()
client.close()

# https://www.blog.pythonlibrary.org/2021/09/05/pysimplegui-using-an-image-as-a-button/